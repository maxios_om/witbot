//
//  AppDelegate+Configration.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

extension AppDelegate: Configrable {

	func configure() {
		
		configureLogger()
		configureProxies()
		configureKeyboardManager()
		Application.application.apply(configration: .development)
	}
	
	private func configureProxies() {
		
		let omkarsWitBotProxyConfigration = OmkarsWitBotProxyConfigration(environment: .development,
		                                                                  contentType: .json,
		                                                                  endPoints: OmkarsWitBotProxyEndPoints.default) // swiftlint:disable:this line_length
		
		OmkarsWitBotProxy.configure(omkarsWitBotProxyConfigration)
	}
	
	private func configureLogger() {
		Logger.configure(Logger.LogConfigration.debug)
	}
	
	private func configureKeyboardManager() {
	
		IQKeyboardManager.sharedManager().enable = true

	}
}
