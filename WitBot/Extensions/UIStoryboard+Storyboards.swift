//
//  UIStoryboard+Storyboards.swift
//  Chatbot
//
//  Created by Omkar khedekar on 23/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

extension UIStoryboard {
	
	enum Storyboard: String {
		case main
		case chat
		var filename: String {
			return rawValue.capitalized
		}
	}
	
	convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
		self.init(name: storyboard.filename, bundle: bundle)
	}
	
	class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
		return UIStoryboard(name: storyboard.filename, bundle: bundle)
	}
	
	func instantiateViewController<T: UIViewController>() -> T where T: Identifiable {
		guard let viewController = self.instantiateViewController(withIdentifier: T.identifier) as? T else {
			fatalError("Couldn't instantiate view controller with identifier \(T.identifier) ")
		}
		return viewController
	}
}
