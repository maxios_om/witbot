//
//  SessionManager.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

class Session {

	fileprivate (set) var activeUser: User?
	
	static let session = Session()
	
	func start(user: User?) {
		activeUser = user
	}
	private init() {}
}
