//
//  URL+Additions.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

extension URL {

	static func URL(path: String, baseURL: URL) -> URL? {
		guard let baseUrlComponants = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else { return nil }
		let componants = baseUrlComponants
		return componants.url?.appendingPathComponent(path)
	}
}
