//
//  Logger.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

public class Logger {
	
	public enum LogLevels {
		case `default`
		case error
		case info
	}
	
	public enum LogConfigration {
		case debug	// swiftlint:disable:this identifier_name
		case errorsOnly
		case off // swiftlint:disable:this identifier_name
	}

	fileprivate static var configration: LogConfigration = .debug
	
}

extension Logger {

	public static func log(_ message: Any?, logLevel: LogLevels = LogLevels.default) {
		
		if configration == .off {
			return
		}
		
		var seperator: String = ""
		var terminator: String = ""
		
		switch logLevel {
		case .error:
			seperator = "\n"
			terminator = "\r"
		case .info:
			seperator = "\n"
			terminator = "\r"
		default:
			seperator = ""
			terminator = "\r"
		}
		
		switch configration {
		case .debug: _log(message ?? "", separator: seperator, terminator: terminator)
		case .errorsOnly:
			if logLevel == .error {
				_log(message ?? "", separator: seperator, terminator: terminator)
			}
			
		default: break
			
		}
	}

	public static func configure(_ configration: LogConfigration = .debug) {
		Logger.configration = configration
	}
	
	private static func _log(_ items: Any..., separator: String = "-", terminator: String = "\n") {
		
		debugPrint(items, separator: separator, terminator: terminator)
	}
}
