//
//  UIdevice+Platform.swift
//  WitBot
//
//  Created by Omkar khedekar on 13/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

extension UIDevice {
	static var isSimulator: Bool {
		return ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil
	}
}
