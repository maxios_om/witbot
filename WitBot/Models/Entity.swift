//
//  Entity.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

class Entity {
	fileprivate enum ResponseJSONFields {
		static let confidence = "confidence"
		static let value = "value"
		static let type = "type"
		static let suggested = "suggested"
	}
	
	var entityName: String?
	var type: String?
	var value: String?
	var suggested: Bool = false
	var confidence: Double?
}

extension Entity: Boxable {

	typealias ModelType = Entity
	
	static func box(_ dictionery: [String : AnyHashable]) -> Entity {
		
		guard
			let confidence = dictionery[Entity.ResponseJSONFields.confidence] as? Double,
			let value = dictionery[Entity.ResponseJSONFields.value] as? String
		else { return Entity() }
		
		let entity = Entity()
		entity.value = value
		entity.confidence = confidence
		if let type = dictionery[Entity.ResponseJSONFields.type] as? String {
			entity.type = type
		}
		
		if let suggested = dictionery[Entity.ResponseJSONFields.suggested] as? Bool {
				entity.suggested = suggested
		}
		return entity
	}
}
