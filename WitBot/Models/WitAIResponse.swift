//
//  WitAIResponse.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct WitAIResponse: JSONResourceType {
	typealias Model = BotResponse
	
}

extension WitAIResponse {

	func model(jsonDictionary: [String : AnyHashable]) -> BotResponse? {
		return BotResponse.box(jsonDictionary)
	}
}
