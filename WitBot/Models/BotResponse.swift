//
//  BotResponse.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

class BotResponse: Boxable, Messagable {
	typealias ModelType = BotResponse
	
	fileprivate enum ResponseJSONFields {
		static let messageID = "msg_id"
		static let queryText = "query"
		static let entities = "entities"
	}
	
	var queryText: String? = ""
	var message: String? = ""
	var entities: [Entity]?
	var type: MessageType = .unknown
}

extension BotResponse {

	static func box(_ dictionery: [String : AnyHashable]) -> BotResponse {
		guard
			let queryText = dictionery[BotResponse.ResponseJSONFields.queryText] as? String
			else {
			Logger.log("Message id or queryText is nil", logLevel: Logger.LogLevels.error)
			return BotResponse()
		}
		
		let instance = BotResponse()
		instance.queryText = queryText
		
		if let entityJSON = dictionery[BotResponse.ResponseJSONFields.entities] as? [String : AnyHashable] {
			
			var _entities: [Entity] = []
			
			for (key, value) in entityJSON {
				Logger.log(key)
				Logger.log(value)
				if let array = value as? [[String : AnyHashable]] {
					let some = array.map({ (value: [String : AnyHashable]) -> Entity in
						let model = Entity.box(value)
						model.entityName = key
						return model
					})
					Logger.log(some)
					_entities.append(contentsOf: some)
				}
			}
			instance.entities = _entities
		}
		
		return instance
	}
}
