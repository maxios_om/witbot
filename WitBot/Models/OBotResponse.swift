//
//  OBotResponse.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct OBotResponse: Messagable {
	
	fileprivate enum ResponseJSONFields {
		static let message = "message"
		static let queryText = "query"
	}
	
	var message: String?
	var queryText: String?
	var type: MessageType = .unknown
}

extension OBotResponse: Boxable {

	typealias ModelType = OBotResponse
	
	static func box(_ dictionery: [String : AnyHashable]) -> OBotResponse {
		
		var message: String? = nil
		var queryText: String? = nil
		
		if let _message = dictionery[OBotResponse.ResponseJSONFields.message] as? String {
			message = _message
		}
		
		if let _queryText = dictionery[OBotResponse.ResponseJSONFields.queryText] as? String {
			queryText = _queryText
		}
		
		return OBotResponse(message: message,
		                    queryText: queryText,
		                    type: .reply)
	}
}

extension OBotResponse: JSONResourceType {
	
	typealias Model = OBotResponse
	func model(jsonDictionary: [String : AnyHashable]) -> OBotResponse? {
		return OBotResponse.box(jsonDictionary)
	}
}
