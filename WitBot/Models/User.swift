//
//  User.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct User {

	fileprivate enum ResponseJSONFields {
		static let id = "id"
		static let name = "name"
	}
	
	var id: String?
	var name: String?
}

extension User: Boxable {

	typealias ModelType = User
	
	static func box(_ dictionery: [String : AnyHashable]) -> User {
		
		var id: String? = nil
		var name: String? = nil
		
		if let _name = dictionery[ResponseJSONFields.name] as? String {
			name = _name
		}
		
		if let _id = dictionery[ResponseJSONFields.id] as? String {
			id = _id
		}
		
		return User(id: id, name: name)
	}
}
