//
//  ViewController.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import SVProgressHUD

class ViewController: UIViewController {
	
	@IBOutlet weak fileprivate var loginWithFB: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configure()
	}
}

extension ViewController {

	@IBAction func loginTapped(_ sender: UIButton) {
		
		if !UIDevice.isSimulator {
			let loginManager = LoginManager()
			
			loginManager.logIn([.email, .publicProfile], viewController: self) { [weak self] (loginResult) in
				
				switch loginResult {
				case .failed(let error):
					Logger.log(error, logLevel: Logger.LogLevels.error)
				case .cancelled:
					Logger.log("User cancelled login.")
				case .success(let grantedPermissions, let declinedPermissions, let accessToken):
					Logger.log(grantedPermissions)
					Logger.log(declinedPermissions)
					Logger.log(accessToken)
					self?.fetchDetails()
				}
			}
		} else {
			navigateToChat()
		}
	}
	
	@IBAction func skipTapped(_ sender: UIButton) {
		
		navigateToChat()
	}
}

extension ViewController: Configrable {

	func configure() {
	
		if AccessToken.current != nil {
			loginWithFB.isEnabled = false
			fetchDetails()
		} else {
			loginWithFB.isEnabled = true
		}
		
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
}

extension ViewController {

	fileprivate func fetchDetails() {
	
		SVProgressHUD.show()
		
		FBProxy.fetchMe({ [weak self] (result) in
			Logger.log(result)
			switch result {
			case .success(let user):
				Logger.log(user)
				
				Session.session.start(user: user)
				DispatchQueue.main.async {
					SVProgressHUD.dismiss()
					self?.navigateToChat()
				}
				
			case .failure(let error):
				Logger.log(error)
			}
		})
	}
	
	fileprivate func navigateToChat() {
		
		let chatStoryboard = UIStoryboard.storyboard(.chat)
		
		guard // swiftlint:disable:next line_length
			let chatViewController = chatStoryboard.instantiateViewController(withIdentifier: ChatViewController.identifier) as? ChatViewController
			else {
				fatalError("unable to instantiate ChatViewController")
		}
		navigationController?.pushViewController(chatViewController, animated: true)
	}
}
