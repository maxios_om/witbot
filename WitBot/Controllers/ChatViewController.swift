//
//  ChatViewController.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import SVProgressHUD
import UIKit

class ChatViewController: UIViewController {
	
	@IBOutlet weak var chatTableView: UITableView!

	fileprivate var messages: [OBotResponse] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		configure()
		processQuery(query: "Hello")
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		navigationItem.backBarButtonItem = nil
		navigationItem.leftBarButtonItem = nil
	}
}

extension ChatViewController: Configrable {
	
	func configure() {
		
		title = "Chit chat"
		navigationController?.setNavigationBarHidden(false, animated: false)
		
		chatTableView.estimatedRowHeight = 80
		chatTableView.rowHeight = UITableViewAutomaticDimension
		chatTableView.estimatedSectionFooterHeight = 44
		
		chatTableView.register(ChatTableViewCell.nib,
		                       forCellReuseIdentifier: ChatTableViewCell.identifier)
		
		chatTableView.register(ChatInputTableFooterView.nib,
		                       forHeaderFooterViewReuseIdentifier: ChatInputTableFooterView.identifier)
	}
}

extension ChatViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return messages.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: ChatTableViewCell.identifier, for: indexPath)
		
		if let cell = cell as? ChatTableViewCell {
			cell.configure(message: messages[indexPath.row])
		}
		
		return cell
	}
}

extension ChatViewController : UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 44
	}
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		// swiftlint:disable:next line_length
		if let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ChatInputTableFooterView.identifier) as? ChatInputTableFooterView {
			let onSendHandelr: ChatInputTableFooterView.SendHadler = {[weak self] (text) in
				guard let query = text else { return }
				self?.processQuery(query: query)
				
			}
			view.onSend = onSendHandelr
			return view
		}
		return nil
	}
}

extension ChatViewController {

	fileprivate func processQuery(query: String?) {
		
		guard let query = query else { return }
		
		self.messages.append(OBotResponse(message: nil, queryText: query, type: .query))
		self.chatTableView.reloadData()
		self.scrollToLastRow()
		SVProgressHUD.show()
		let request = OBotRequest(query: query)
		OmkarsWitBotProxy.execute(request) { [weak self ](result) in
			
			DispatchQueue.main.async {
				switch result {
				case .success(let response):
					self?.messages.append(response)
					self?.chatTableView.reloadData()
				case .failure(let error):
					Logger.log(error, logLevel: Logger.LogLevels.error)
				}
				self?.scrollToLastRow()
				SVProgressHUD.dismiss()
			}
		}
	}
	
	fileprivate func scrollToLastRow() {
		let indexPath = IndexPath(row: messages.count - 1, section: 0)
		self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
}
