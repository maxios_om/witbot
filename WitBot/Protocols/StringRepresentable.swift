//
//  StringRepresentable.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

protocol StringRepresentable {
	
	var asString: String { get }
}

extension StringRepresentable {

	var asString: String {
	
		return String(describing: self)
	}
}
