//
//  AppConfigrations.swift
//  WitBot
//
//  Created by Omkar khedekar on 13/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

enum AppEnvironment {
	case development
	case staging
	case live
}

protocol AppEnvironmentReflectable {

	var reflection: AppEnvironment { get }
}

protocol ApplicationSettings {
	
	static var application: Self { get }
	var configration: AppEnvironment { get set }
	mutating func apply(configration: AppEnvironment)
}

struct Application: ApplicationSettings {
	mutating func apply(configration: AppEnvironment) {
		self.configration = configration
	}
	
	var configration: AppEnvironment
	
	static var application: Application = Application(configration: .development)
}
