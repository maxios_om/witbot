//
//  NetworkProxy.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

enum ProxyEnvironment {
	case development
	case staging
	case live
}

enum CommonHeaderFields: String {
	case authorization = "Authorization"
	case contentType = "Content-Type"
	case contentLength = "Content-Length"
	case contentMD5 = "Content-MD5"
	case acceptCharset = "Accept-Charset"
	case acceptEncoding = "Accept-Encoding"
	case date = "Date"
	case userAgent = "User-Agent"
}

enum ContentType: String {
	
	case javascript = "application/javascript"
	case json = "application/json"
	case formUrlEncoded = "application/x-www-form-urlencoded"
	case xml = "application/xml" // swiftlint:disable:this identifier_name
	case zip = "application/zip" // swiftlint:disable:this identifier_name
	case pdf = "application/pdf" // swiftlint:disable:this identifier_name
	case mpeg = "audio/mpeg"
	case vorbis = "audio/vorbis"
	case formData = "multipart/form-data"
	case css = "text/css" // swiftlint:disable:this identifier_name
	case html = "text/html"
	case plain = "text/plain"
	case png = "image/png" // swiftlint:disable:this identifier_name
	case jpeg = "image/jpeg"
	case gif = "image/gif" // swiftlint:disable:this identifier_name
	
}

protocol EndPointCustomizable {
	
	var development: URL? { get }
	var staging: URL? { get }
	var live: URL? { get }
}

protocol ProxyConfigration {
	
	var environment: ProxyEnvironment { get set }
	var contentType: ContentType { get set }
	var endPoints: EndPointCustomizable { get }
}

protocol NetWorkProxy {
	
	associatedtype RequestType: NetworkRequest
	associatedtype ResponseType: ResourceType
	
	static var configration: ProxyConfigration? { get set }
	
	static var baseURL: URL? { get }
	static func configure(_ environment: ProxyConfigration)
	static func execute(_ request: RequestType, completion: @escaping (Result<ResponseType.Model>) -> Void)
	
}

extension NetWorkProxy {

	static func configure(_ configration: ProxyConfigration) {
		self.configration = configration
	}

	static var baseURL: URL? {
		guard let configration = configration else { return nil }
		switch configration.environment {
		case .development: return configration.endPoints.development
		case .staging: return configration.endPoints.staging
		case .live: return configration.endPoints.live
		}
	}
}
