//
//  Messagable.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

enum MessageType {
	case query
	case reply
	case unknown
}

protocol Messagable {

	var queryText: String? { set get }
	var message: String? { set get }
	var type: MessageType { set get }
}
