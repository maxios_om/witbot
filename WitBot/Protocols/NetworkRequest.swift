//
//  NetworkRequest.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

typealias JsonBodyType = [String:AnyHashable]

protocol NetworkRequest {

	var path: String { set get }
}

protocol HTTPRequest: NetworkRequest {
	
	var method: HTTPMethod { get }
	var headers: [String:String]? { get }
	var JSONBody: JsonBodyType? { get }
	func buildRequest(_ baseURL: URL) -> URLRequest?
	
	init(path: String, method: HTTPMethod, headers: [String:String]?, JSONBody: JsonBodyType?)
}

extension HTTPRequest {
	
	var method: HTTPMethod { return .get }
	var headers: [String : String]? { return nil }
	var JSONBody: AnyObject? { return nil }
	var path: String { return "" }
	
	func buildRequest(_ baseURL: URL) -> URLRequest? {
		
		guard
			let url = URL.URL(path: path, baseURL: baseURL)
		else { return nil }
		
		var request = URLRequest(url: url)
		
		request.httpMethod = method.rawValue
		
		if let headers = headers {
			for (headerField, value) in headers {
				request.setValue(value, forHTTPHeaderField: headerField)
			}
		}
		
		if let body = JSONBody {
			let json = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
			Logger.log(json)
			request.httpBody = json
		}
		
		return request
	}
}
