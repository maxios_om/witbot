//
//  Configrable.swift
//  Chatbot
//
//  Created by Omkar khedekar on 23/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

protocol Configrable {
	func configure()
}
