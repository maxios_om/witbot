//
//  ResourceType.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

protocol ResourceType {

	associatedtype Model:Boxable
}

protocol JSONResourceType: ResourceType {

	func model(jsonDictionary: [String : AnyHashable]) -> Model?
}

extension JSONResourceType {

	func model(jsonDictionary: [String : AnyHashable]) -> Model? { return nil }

}

extension JSONResourceType {

	func result(from data: Any?) -> Result<Model> {

		guard let data = data else { return .failure(ParsingError.invalidJSONData) }
		debugPrint(data)
		
		guard let jsonObject = parse(some: data) else { return .failure(ParsingError.invalidJSONData) }
		
		if let jsonDictionary = jsonObject as? [String: AnyHashable] {
			return resultFrom(jsonDictionary: jsonDictionary)
		}

		return .failure(ParsingError.unsupportedType)
	}
}

fileprivate extension JSONResourceType {
	
	func parse(some: Any) -> Any? {
		
		if let asData = some as? Data {
			return parse(data: asData)
		}
		
		if let asString = some as? String {
			return parse(string: asString)
		}
		
		if let asJSONDict = some as? [String: AnyHashable] {
			return asJSONDict
		}
		
		if let asJSONArray = some as? [AnyHashable] {
			return asJSONArray
		}
		
		return nil
	}
	
	func parse(data: Data) -> Any? {
		
		guard
			let jsonObject = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
			else {
			return nil
		}
		
		if let jsonDictionary = jsonObject as? [String: AnyHashable] {
			return jsonDictionary
		}
		
		if let jsonArray = jsonObject as? [AnyHashable] {
			return jsonArray
		}
		
		return nil
	}
	
	func parse(string: String) -> Any? {
		
		guard let data = string.data(using: .utf8, allowLossyConversion: true) else {
			return nil
		}
		
		return parse(data: data)
	}
	
	func resultFrom(jsonDictionary: [String: AnyHashable]) -> Result<Model> {
		if let parsedResults = model(jsonDictionary: jsonDictionary) {
			return .success(parsedResults)
		} else {
			return .failure(ParsingError.cannotParseJSONDictionary)
		}
	}
}
