//
//  Boxable.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

protocol Boxable {
	associatedtype ModelType
	static func box (_ dictionery: [String:AnyHashable]) -> ModelType
	func unBox(_ type: ModelType) -> [String:AnyHashable]?
}

extension Boxable {

	func unBox(_ type: ModelType) -> [String:AnyHashable]? { return nil }
}
