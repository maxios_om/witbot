//
//  StoryboardIdentifiable.swift
//  Chatbot
//
//  Created by Omkar khedekar on 23/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

protocol Identifiable {
	static var identifier: String { get }
}

extension Identifiable where Self: UIViewController {
	static var identifier: String {
		let format = "com.withbot."
		return format + String(describing: self.classForCoder())
	}
}

extension Identifiable where Self: UIView {
	static var identifier: String {
		let format = "com.withbot."
		return format + String(describing: self.classForCoder())
	}
}
