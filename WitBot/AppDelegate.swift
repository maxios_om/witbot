//
//  AppDelegate.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import FBSDKCoreKit
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication,
	                 didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool { // swiftlint:disable:this line_length

		FBSDKApplicationDelegate.sharedInstance().application(application,
		                                                      didFinishLaunchingWithOptions: launchOptions)
		configure()
		
		return true
	}
	
	func application(_ app: UIApplication, // swiftlint:disable:this identifier_name
	                 open url: URL, // swiftlint:disable:this identifier_name
	                 options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		
		let handled = FBSDKApplicationDelegate.sharedInstance().application(app,
		                                                                    open: url,
		                                                                    options: options)
		
		return handled
	}
	
	func application(_ application: UIApplication,
	                 open url: URL, // swiftlint:disable:this identifier_name
	                 sourceApplication: String?,
	                 annotation: Any) -> Bool { 
		
		let handled = FBSDKApplicationDelegate.sharedInstance().application(application,
		                                                                    open: url,
		                                                                    sourceApplication: sourceApplication,
		                                                                    annotation: annotation)
		
		return handled
	}
}
