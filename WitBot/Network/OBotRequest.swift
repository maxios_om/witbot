//
//  OBotRequest.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation
struct OBotRequest: HTTPRequest {
	
	enum JSONFields {
		static let query = "query"
	}
	
	enum Path: String {
		case `default` = "ask"
	}
	
	typealias Resource = OBotResponse
	
	var path: String
	var method: HTTPMethod
	var headers: [String : String]?
	var JSONBody: JsonBodyType?
}

extension OBotRequest {
	
	init(path: Path = .default, method: HTTPMethod = .get, JSONBody: JsonBodyType?) {
		self.init(path: path.rawValue, method: method, headers: nil, JSONBody: JSONBody)
	}
	
	init(query: String) {
		let json = [JSONFields.query: query]
		self.init(path: Path.default.rawValue,
		          method: .post,
		          headers: [CommonHeaderFields.contentType.rawValue: ContentType.json.rawValue],
		          JSONBody: json)
	}
}
