//
//  FBProxy.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import FacebookCore
import Foundation

struct FBProxy {
	
	typealias ComplitionHandler = ((Result<User>) -> Void)
	
	static func fetchMe(_ complition: ComplitionHandler?) {
		
		let connection = GraphRequestConnection()
		connection.add(GraphRequest(graphPath: "/me")) { (_ httpResponse, _ result) in
			
			guard let complition = complition else { return }
			
			switch result {
			case .success(let response):
				print("Graph Request Succeeded: \(response)")
				if let response = response.dictionaryValue as? [String: AnyHashable] {
					complition(Result.success(User.box(response)))
				}
			case .failed(let error):
				complition(Result.failure(error))
			}
			
		}
		connection.start()
	}
}
