//
//  OmkarsWitBotProxyConfigration.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct OmkarsWitBotProxyConfigration: ProxyConfigration {
	
	var environment: ProxyEnvironment
	var contentType: ContentType
	var endPoints: EndPointCustomizable
}
