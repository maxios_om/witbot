//
//  WitAIProxy.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

class WitAIProxy {

	typealias RequestType = WitAIRequest
	typealias ResponseType = WitAIResponse
	
	static var baseURL: URL? {
		return URL(string: "https://api.wit.ai/message?q=")
	}
	
	static func execute(_ request: WitAIRequest, completion: @escaping (Result<BotResponse>) -> Void) {
		
		guard
			let baseURL = baseURL,
			let urlRequest = request.buildRequest(baseURL) else {
			return
		}

		let onComplition: ApiManager.ComplitionHandler = { (data, error) in
			
			Logger.log(data)
			Logger.log(error, logLevel: Logger.LogLevels.error)
			let result = ResponseType().result(from: data)
			completion(result)
		}
		
		ApiManager.execute(request: urlRequest,
		                   onComplition: onComplition)
	}
}
