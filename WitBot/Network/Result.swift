//
//  Result.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

public enum Result<Template> {
	case success(Template)
	case failure(Error)
}

enum ParsingError: Error {
	case invalidJSONData
	case cannotParseJSONDictionary
	case cannotParseJSONArray
	case unsupportedType
}
