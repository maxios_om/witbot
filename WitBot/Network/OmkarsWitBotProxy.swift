//
//  OmkarsWitBotProxy.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

class OmkarsWitBotProxy: NetWorkProxy {
	
	typealias RequestType = OBotRequest
	typealias ResponseType = OBotResponse
	
	static var configration: ProxyConfigration?
	
	static func execute(_ request: OBotRequest, completion: @escaping (Result<OBotResponse>) -> Void) {
	
		guard
			let baseURL = baseURL,
			let urlRequest = request.buildRequest(baseURL) else {
				return
		}
		
		let onComplition: ApiManager.ComplitionHandler = { (data, error) in
			
			let result: Result<OBotResponse.Model>
			
			if let error = error {
				
				Logger.log(error, logLevel: Logger.LogLevels.error)
				result = Result<OBotResponse.Model>.failure(error)
				
			} else {
				
				Logger.log(data)
				result = ResponseType().result(from: data)
			}

			completion(result)
		}
		
		ApiManager.execute(request: urlRequest,
		                   onComplition: onComplition)
	}
}
