//
//  ApiManager.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

class ApiManager {
	
	typealias ComplitionHandler = (Data?, Error?) -> Void
	
	fileprivate struct Session {
		static let configration =  URLSessionConfiguration.default
		static let session = URLSession(configuration: Session.configration)
	}
}

extension ApiManager {

	static func execute(request: URLRequest, onComplition: ComplitionHandler?) {
		
		let task = Session.session.dataTask(with: request) { (_ data, _ response, _ error) in
			
			if let onComplition = onComplition {
				onComplition(data, error)
			}
		}
		
		task.resume()
	}
}
