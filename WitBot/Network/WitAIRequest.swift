//
//  WitAIRequest.swift
//  WitBot
//
//  Created by Omkar khedekar on 30/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct WitAIRequest: HTTPRequest {
	
	enum Headers {
		static let `default` = ["Authorization": "Bearer A4CUEUDGU6F3MVSCTDPWEVFPRQ7KTXKP"]
	}
	
	typealias Resource = WitAIResponse
	
	var path: String
	var headers: [String : String]?
	var JSONBody: JsonBodyType?
	var method: HTTPMethod
	
	func buildRequest(_ baseURL: URL) -> URLRequest? {
		
		let reqPath = baseURL.absoluteString + path
		guard
			let url = URL(string: reqPath)
			else { return nil }
		
		var request = URLRequest(url: url)
		
		request.httpMethod = method.rawValue
		
		if let headers = headers {
			for (headerField, value) in headers {
				request.setValue(value, forHTTPHeaderField: headerField)
			}
		}
		
		if let body = JSONBody {
			request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
		}
		
		return request
	}
}

extension WitAIRequest {
	
	init(path: String, method: HTTPMethod = .get,
	     headers: [String:String]? = nil,
	     JSONBody: JsonBodyType? = nil) {
		self.path = path
		self.headers = headers
		self.method = method
		self.JSONBody = JSONBody
	}
	
	init(path: String, headers: [String : String]? = WitAIRequest.Headers.default) {
		self.init(path: path, method: .get, headers: headers, JSONBody: nil)
	}
	
}
