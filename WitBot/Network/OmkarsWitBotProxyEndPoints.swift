//
//  OmkarsWitBotProxyEndPoints.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

struct OmkarsWitBotProxyEndPoints: EndPointCustomizable {
	
	private enum EndPoints: String {
		case local = "http://localhost:3000/"
		case live = "https://omkars-wit-bot.herokuapp.com/"
	}
	
	var development: URL? {
		return URL(string: EndPoints.local.rawValue)
	}
	
	var staging: URL? {
		return URL(string: EndPoints.live.rawValue)
	}
	
	var live: URL? {
		return URL(string: EndPoints.live.rawValue)
	}
	
	static let `default` = OmkarsWitBotProxyEndPoints()
}
