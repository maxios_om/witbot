//
//  HTTPMethod.swift
//  WitBot
//
//  Created by Omkar khedekar on 29/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation
// swiftlint:disable identifier_name

enum HTTPMethod: String {
	
	case get = "GET"
	case post = "POST"
	case put = "PUT"
	case patch = "PATCH"
	case delete = "DELETE"
}
