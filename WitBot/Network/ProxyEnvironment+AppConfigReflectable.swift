//
//  ProxyEnvironment+AppConfigReflectable.swift
//  WitBot
//
//  Created by Omkar khedekar on 13/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import Foundation

extension ProxyEnvironment: AppEnvironmentReflectable {
	
	var reflection: AppEnvironment {
		switch self {
		case .development: return .development
		case .live: return .live
		case .staging: return .staging
		}
	}
}
