//
//  ChatTableViewCell.swift
//  WitBot
//
//  Created by Omkar khedekar on 02/05/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell, Identifiable {

	@IBOutlet fileprivate weak var name: UILabel!
	@IBOutlet fileprivate weak var message: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		configure()
    }
}

extension ChatTableViewCell: Configrable {

	func configure() {
		selectionStyle = .none
	}
	
	func configure(message: OBotResponse) {
		
		switch message.type {
		case .query:
			name.text = Session.session.activeUser?.name
			name.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
			name.textAlignment = .right
			self.message.textAlignment = .right
			self.message.text = message.queryText
			
		case .reply:
			name.text = "Botoba"
			name.textColor = #colorLiteral(red: 0, green: 0.9810667634, blue: 0.5736914277, alpha: 1)
			name.textAlignment = .left
			self.message.textAlignment = .left
			self.message.text = message.message

		case .unknown:
			name.text = "Gods speaking"
			name.textColor = #colorLiteral(red: 0.6717925668, green: 0.0968252793, blue: 0.2592633367, alpha: 1)
			name.textAlignment = .left
			self.message.textAlignment = .left
			self.message.text = "Devil listening"
		}
	}
}
