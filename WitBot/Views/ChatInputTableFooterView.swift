//
//  ChatInputTableFooterView.swift
//  Chatbot
//
//  Created by Omkar khedekar on 23/04/17.
//  Copyright © 2017 UnitedByHCL. All rights reserved.
//

import UIKit

class ChatInputTableFooterView: UITableViewHeaderFooterView, Identifiable {

	typealias SendHadler = (String?) -> Void
	typealias DidStartEditingHandler = (Void) -> Void
	@IBOutlet fileprivate weak var chatBox: UITextField!

	fileprivate lazy var sendButton = { () -> UIButton in
		let button = UIButton(frame: CGRect(x: 0, y: 1, width: 40, height: 40))
		button.setTitle("Send", for: .normal)
		button.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
		button.backgroundColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
		button.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
		return button
	} ()
	var onSend: SendHadler?
	var onBeginEditing: DidStartEditingHandler?
	override func awakeFromNib() {
		super.awakeFromNib()
		configure()
	}
}

extension ChatInputTableFooterView : UITextFieldDelegate {

	func textFieldDidBeginEditing(_ textField: UITextField) {
		if let onBeginEditing = onBeginEditing {
			onBeginEditing()
		}
	}
}

extension ChatInputTableFooterView {
	func sendButtonTapped() {
		chatBox.endEditing(true)
		if let onSend = onSend {
			let text = chatBox.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
			chatBox.text = nil
			onSend(text)
		}
	}
}

extension ChatInputTableFooterView: Configrable {

	func configure() {
		chatBox.rightViewMode = .always
		chatBox.rightView = sendButton
		chatBox.placeholder = "Talk to me am here for help"
		chatBox.delegate = self
	}
}
